# Fluxograma

O fluxograma tem como objetivo ser uma explicação visual do processo de tradução das págians web. O fluxograma fica na página [WebWML#Fluxograma](https://wiki.debian.org/Brasil/Traduzir/WebWML#Fluxograma_de_tradu.2BAOcA4w-o_de_p.2BAOE-ginas_web). As instruções gerais para traduzir os outros documentos relativos ao Debian ficam em [Brasil Traduzir WebWML](https://wiki.debian.org/Brasil/Traduzir). A explicação da organização do site do Debian fica em [Helping with the Debian web pages](https://www.debian.org/devel/website/index.en.html).


## Arquivos
O arquivo principal é o **Fluxograma.svg**.

 * **Fluxograma.svg** arquivo principal e é o resultado principal dessa iniciativa. Foi criado com o programa Inkscape.
 * **Flowchart.svg** tradução para o inglês do arquivo Fluxograma.svg.
 * **Fluxo.odg** planejamento inicial do fluxograma, seus textos estão no arquivo Fluxo.txt.
 * **Fluxo.txt** textos usados no planejamento e tem a mesma numeração que o Fluxo.odg.
 * **build.sh** exporta os arquivos Fluxo.svg e Flowchart.svg para os formatos .pdf e .png na pasta public/. Usado pelo CI/CD.
