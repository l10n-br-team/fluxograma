#!/bin/bash

mkdir -p public

echo "<html><head><title>File list</title></head><body>" > public/index.html
for i in `ls *.svg`;do 
        j="${i%.*}";
        inkscape --export-dpi=300 --export-pdf=public/$j.pdf --export-png=public/$j.png $j.svg;
        cp $i public/$i;
        echo "<a href=\"$j.svg\">$j.svg</a><br><a href=\"$j.png\">$j.png</a><br><a href=\"$j.pdf\">$j.pdf</a><br>" >> public/index.html
done
echo "</body></html>" >> public/index.html
